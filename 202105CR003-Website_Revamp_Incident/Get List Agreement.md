curl API

>curl --location --request GET 'https://devbackend.bfi.co.id/leads_api/api/v1/webrevamp/list_agreement?phone_number=08121303530&order_by=TanggalJatuhTempo%20asc&page=1&size=10' \
>--header 'accept: text/plain'

sql query

```sql
Declare 
@OrderBy varchar(25) = 'TanggalJatuhTempo asc',
@FirstRec int = 1,
@LastRec int = 10,
@SearchByText varchar(20) = '08121303530' --phone number konsumen

select 
  * 
from 
  (
    SELECT 
      ROW_NUMBER() over (
        order by 
          case when @OrderBy = 'ContractNumber asc' then ContractNumber end asc, 
          case when @OrderBy = 'ContractNumber desc' then ContractNumber end desc, 
          case when @OrderBy = 'JenisKredit asc' then JenisKredit end asc, 
          case when @OrderBy = 'JenisKredit desc' then JenisKredit end desc, 
          case when @OrderBy = 'AngsuranPerbulan asc' then AngsuranPerbulan end asc, 
          case when @OrderBy = 'AngsuranPerbulan desc' then AngsuranPerbulan end desc, 
          case when @OrderBy = 'TanggalJatuhTempo asc' then TanggalJatuhTempo end asc, 
          case when @OrderBy = 'TanggalJatuhTempo desc' then TanggalJatuhTempo end desc,
		  case when @OrderBy = 'AgreementDate desc' then AgreementDate end desc,
		  case when @OrderBy = 'AgreementDate asc' then AgreementDate end asc
      ) RowNumber, 
		ContractNumber,
		JenisKredit,
		AngsuranPerbulan,
		TanggalJatuhTempo,
		MobilePhone,
		LicensePlate
    FROM 
      (
        select 
          a.ContractNumber, 
          a.JenisKredit, 
          a.AngsuranPerbulan, 
          a.NextInstallmentDate TanggalJatuhTempo,
		  a.MobilePhone,
		  a.AgreementDate,
		  a.LicensePlate

        FROM 
          BFIDataAggregation.dbo.Lead_ListAgreement a with(nolock) 
        WHERE   
	   case when LEFT(a.mobilephone,3) = '628'
				then SUBSTRING(a.mobilephone, 4, LEN(a.mobilephone)-3)
			when LEFT(a.mobilephone,2) = '08'
				then SUBSTRING(a.mobilephone, 3, LEN(a.mobilephone)-2)
			when LEFT(a.mobilephone,1) = '8'
				then SUBSTRING(a.mobilephone, 2, LEN(a.mobilephone)-1)
		  else
		  a.mobilephone end
	   = case when LEFT(@SearchByText,3) = '628'
				then SUBSTRING(@SearchByText, 4, LEN(@SearchByText)-3)
			when LEFT(@SearchByText,2) = '08'
				then SUBSTRING(@SearchByText, 3, LEN(@SearchByText)-2)
			when LEFT(@SearchByText,1) = '8'
				then SUBSTRING(@SearchByText, 2, LEN(@SearchByText)-1)
		  else
		  @SearchByText end
      ) aa
  ) aa 
where 
aa.RowNumber between @FirstRec and @LastRec 



SELECT 
  count('') TotalRecord 
FROM 
  (
    SELECT 
      a.ContractNumber, 
      a.JenisKredit, 
      a.AngsuranPerbulan, 
      a.TanggalJatuhTempo,
	  a.LicensePlate
    FROM 
      BFIDataAggregation.dbo.Lead_ListAgreement a with(nolock) 
    WHERE 
     case when LEFT(a.mobilephone,3) = '628'
				then SUBSTRING(a.mobilephone, 4, LEN(a.mobilephone)-3)
			when LEFT(a.mobilephone,2) = '08'
				then SUBSTRING(a.mobilephone, 3, LEN(a.mobilephone)-2)
			when LEFT(a.mobilephone,1) = '8'
				then SUBSTRING(a.mobilephone, 2, LEN(a.mobilephone)-1)
		  else
		  a.mobilephone end
	   = case when LEFT(@SearchByText,3) = '628'
				then SUBSTRING(@SearchByText, 4, LEN(@SearchByText)-3)
			when LEFT(@SearchByText,2) = '08'
				then SUBSTRING(@SearchByText, 3, LEN(@SearchByText)-2)
			when LEFT(@SearchByText,1) = '8'
				then SUBSTRING(@SearchByText, 2, LEN(@SearchByText)-1)
		  else
		  @SearchByText end
  ) aa

```

