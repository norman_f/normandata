curl

1. Legacy API

>curl --location --request GET 'http://devbackend.bfi.co.id/legacy_api/api/v1/webrevamp/dtl_agreement?contract_number=5210023405' \
>--header 'accept: text/plain'

```sql
DECLARE 
@AgreementNo varchar (25) = '5210023405', -- contractNumber
@ContractStatus varchar(25) = '',
@BDcurrent varchar(25) = '2020-10-09 00:00:00.000'
-- how to get @BDcurrent
select bdcurrent BusinessDate from bfidb..SystemControlCoy with(nolock)

select	agr.AgreementNo, 
		agr.ApplicationID,
		agr.GoLiveDate,
		cu.CustomerID,
		cu.Name CustomerName,
		agr.ContractStatus,
		agr.NTF,
		agr.BranchID,
		agr.InstallmentAmount, 
		js.GroupScheme ProductType, 
		agrs.AssetDescription,
		b.BranchFullName BranchName,
		ISNULL(BFIDB.dbo.fnCekOverDueAmount(agr.BranchID,agr.ApplicationID,@BDcurrent,@BDcurrent),0) - agr.ContractPrepaidAmount as InstallmentDue,
		agr.NextInstallmentDate,
		agr.NextInstallmentDueDate,
		bfidb.dbo.FnCalculationForLCInstallment(agr.ApplicationID, GETDATE()) LCInstallment ,
		ISNULL(ca.EndPastDueDays, 0) EndPastDueDays,
		agr.Tenor,
		agrs.ManufacturingYear,
		agrs.ManufacturingCountry,	
		ac.AttributeContent Warna,
		agrs.LicensePlate,
		agrs.OwnerAddress,
		agr.OutstandingInterest,
		agr.OutstandingPrincipal		
from	bfidb..Agreement agr with(nolock)
		inner join bfidb..AgreementAsset agrs with(nolock) on agr.BranchID = agrs.BranchID and agr.ApplicationID = agrs.ApplicationID
		inner join bfidb..Customer cu with(nolock) on agr.CustomerID = cu.CustomerID
		inner join BFIDB..Branch b with(nolock) on b.BranchID = agr.BranchID
		left join bfidb..collectionagreement ca WITH( nolock) ON ca.applicationid = agr.applicationid AND ca.branchid = agr.branchid 
		left join BFIDB..AssetAttributeContent ac with(nolock) on ac.BranchID = agr.BranchID and ac.ApplicationID = agr.ApplicationID and agrs.AssetSeqNo = ac.AssetSeqNo and ac.AttributeID = 'COLOR'
		inner join BFIDB..ProductOffering po with(nolock) on po.BranchID = agr.BranchID and po.ProductID = agr.ProductID and po.ProductOfferingID = agr.ProductOfferingID
		inner join BFIDB..JournalScheme js with(nolock) on js.JournalSchemeID = po.JournalSchemeID
where	agr.AgreementNo = @AgreementNo and 
		case 
		when agr.ContractStatus = @ContractStatus  then 1
		when @ContractStatus  = '' then 1
		else 0
		end = 1
```

2. Leads API

>curl --location --request GET 'https://devbackend.bfi.co.id/leads_api/api/v1/webrevamp/dtl_agreement?contract_number=5210023405' \
>
>--header 'accept: text/plain'

```sql
http://devbackend.bfi.co.id/legacy_api/api/v1/webrevamp/dtl_agreement?contract_number=5210023405
```

dengan kondisi contract status != expired